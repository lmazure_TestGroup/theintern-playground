*** Settings ***
Documentation    Running an Intern test from Robot Framework
Library    Process
Library    OperatingSystem
Library    XML
Library    squash_tf.TFParamService


*** Keywords ***
Install
    Run Process    npm.cmd    install    cwd=${CURDIR}    stdout=npm.out    stderr=npm.err
    Log File    ${CURDIR}${/}npm.out
    Log File    ${CURDIR}${/}npm.err

Run Test
    [Arguments]   ${test_reference}
    Run Process    npm.cmd    test    grep\=${test_reference}    cwd=${CURDIR}    stdout=npm.out    stderr=npm.err
    Log File    ${CURDIR}${/}npm.out
    ${npm_err} =    Get File    ${CURDIR}${/}npm.err
    ${npm_err_length} =    Get Length    ${npm_err}
    IF    ${npm_err_length}
        Log    ${npm_err}    ERROR
        Fail    The test generated some message in stderr
    END

Parse Surefire Report
    ${root} =    Parse XML    ${CURDIR}${/}surefire_report.xml
    @{failure_elements} =    Get Elements    ${root}    .//testcase/*[.!='grep']
    @{skipped_elements} =    Get Elements    ${root}    .//testcase/*[.='grep']
    @{all_elements} =    Get Elements    ${root}    .//testcase
    ${skipped_count} =    Get length    ${skipped_elements}
    ${total_count} =    Get length    ${all_elements}
    Should Not Be Equal As Integers    ${skipped_count}    ${total_count}    All tests have been skipped
    ${f} =    Set Variable    ${EMPTY}
    FOR    ${failure}    IN    @{failure_elements}
        ${f} =    Catenate    SEPARATOR=\n    ${f}    \ntype: ${failure.tag}\ndetails: ${failure.text}
    END
    Should be empty    ${failure_elements}    An error has been found${f}


*** Test Cases ***
Run Intern Test
    [Documentation]    Run an Intern test
    Install
    ${test_reference} =    Get Test Param    TC_REFERENCE
    Run Test    ${test_reference}
    Parse Surefire Report
