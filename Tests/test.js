const {describe,it,beforeEach,afterEach} = intern.getPlugin("interface.bdd");
const { expect, assert } = intern.getPlugin('chai');

describe('332 demo', () => {
    it("Login to application", async test => {
        const remote = test.remote;
        await remote.get("http://www.google.com")
            .setTimeout("script", 30000)
            .findByXpath("//div[text()='Tout accepter']")
            .click()
            .findByXpath("//a[text()='Gmail']")
            .isDisplayed()
            .then(res => { assert.equal(true, res) }) 
            //.sleep(5000)      //Pour vérifier que le test se déroule comme prévu     
    });
});