// Retrieve the parameters
// Works the same way as
// - squash-tf-services for Robot Framework (https://autom-devops-en.doc.squashtest.com/2022-10/autom/techno/robotframework.html#parameters-usage)
// - opentestfactory-java-param-library for JUnit/Cucumber (https://autom-devops-en.doc.squashtest.com/2022-10/autom/techno/junit.html#parameters-usage)

// ⚠⚠ NOT TESTED YET ⚠⚠

class OpenTFParams {

    #config;

    constructor() {
        const inifile = process.env._SQUASH_TF_TESTCASE_PARAM_FILES;
        const fs = require('fs');
        const ini = require('ini');
        this.config = ini.parse(fs.readFileSync("../" + inifile, "utf-8"));
    }

    getTestParam(paramName, defaultValue) {
        return this.config.test[paramName] ?? defaultValue;
    }

    getGlobalParam(paramName, defaultValue) {
        return this.config.global[paramName] ?? defaultValue;
    }

    getParam(paramName, defaultValue) {
        return this.getTestParam(paramName) ?? this.getGlobalParam(paramName) ?? defaultValue;
    }
}

module.exports = {
    OpenTFParams: OpenTFParams
}